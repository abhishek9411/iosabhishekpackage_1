//
//  File.swift
//  
//
//  Created by Abhishek Singh on 01/07/23.
//

import Foundation
import SwiftUI

@available(iOS 13.0, *)
public struct AbhishekView : View {
    public init(){}
    @available(macOS 10.15.0, *)
    public var body: some View{
        Text("this is first view")
    }
}

public class Addition {
    
    
    public static func addTwoNumber(num1 : Int, num2 : Int) -> Int {
        return num1 + num2
    }
}
